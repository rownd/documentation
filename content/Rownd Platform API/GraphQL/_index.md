---
title: "GraphQL"
date: 2020-06-03T16:37:34-04:00
weight: 14
draft: true
---

# How to use GraphQL

### Some JSON 

    ```json
    [
      {
        "title": "apples",
        "count": [12000, 20000],
        "description": {"text": "...", "sensitive": false}
      },
      {
        "title": "oranges",
        "count": [17500, null],
        "description": {"text": "...", "sensitive": false}
      }
    ]
    ```

Lorem Ipsum.
