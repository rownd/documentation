---
title: "Rownd API Documentation"
date: 2018-12-02T11:02:05+06:00
icon: "ti-sharethis" # themify icon pack : https://themify.me/themify-icons
description: "How to use the Rownd API."
# type don't remove or customize
type : "docs"
# weight: 2
---

### The Rownd API Spec

Full API Documentation: <a href="https://docs.rownd.io/api/">Link to full API Documentation </a>

### A video walk through ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/UNq2-fX0JNQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Easy to use ###

Rownd has an easy to use API for adding and retrieving user data.  

 {{< notice notice >}} Some of the fields will be revoked due to the Rownd Personal Data Center and data ownership constructs.  Developing error handling here is key.... {{</ notice >}}

 Learn more about how to use the API and an example here in the docs.  

