---
title: "How to use Rownd API"
weight: 10
date: 2020-06-03T16:37:34-04:00
draft: false
---

### The Rownd API

Full API Documentation: <a href="https://docs.rownd.io/apiandsdks/#/rest/getting-started">Link to full API Documentation </a>


Rownd has an easy to use API for adding and retrieving user data.  

 {{< notice notice >}} Some of the fields will be revoked due to the Rownd Personal Data Center and data ownership constructs.  Developing error handling here is key.... {{</ notice >}}

### API Walk Through:

#### Create a new user and add data.

Rownd makes this easy with one API call POST to both create a new user (using your user ID system) and adding personal data to rownd.  Let's break down the API:

`PUT https://api.rownd.io/applications/:app/users/:user/data`

**Query Params**: 

**:app**: This is the APP ID, this is created when you create a new application.

**:user**: If you are creating a new user, you can simply add your own userID here.  Have it match your existing user ID structure or system.  If you already have data in Rownd and are updating or adding data, simply enter your existing userID.

**Required Headers**:

**x-rownd-app-key**: This is gathered directly from the App page

**x-rownd-app-secret**: You can only see the app key once prior to it disappearing.  If you lose the app secret, you must create a new App key/secret combo.  

**The Body:**
This is where you pass on personal data to Rownd to protect.  

{{< notice note >}}
 you can only add data-types that have been added to the Rownd Data Mapping.     
{{</ notice >}}

Example of Body: 

```JSON
{
    "data": {
        "email": "testuser@test1.com",
        "first_name": "Robert",
        "last_name": "McAwesome",
        "cell_phone_number": "55555455555"
    }
}
```

Full example (Curl):

```C#
curl --location --request PUT 'https://api.rownd.io/applications/274488958387225088/users/newuser123562/data' \
--header 'x-rownd-app-secret: 6fe6c7c9956ee67c935e89d620b6c912aa12b2b17c1c6b97' \
--header 'Content-Type: application/json' \
--header 'x-rownd-app-key: 13b1f611-327f-478f-8c81-651d3dbae741' \
--data-raw '{
    "data": {
        "email": "testuser@test1.com",
        "first_name": "Robert",
        "last_name": "McAwesome",
        "cell_phone_number": "55555455555"
    }
}'
```
### Retrieving personal data
Okay, that was the hard part... now lets focus on getting data.  There are several ways to get personal data: one user at a time or in bulk. 

{{< notice note >}}
One of the biggest differences here will be data types that are `RESTRICTED`.  These are pieces of personal data that have been revoked by end-users.  If you require that data type, you will hvae to reask for it.  
{{</ notice >}}

#### Retrieve one-users data

First, lets look at the API:
`GET https://api.rownd.io/applications/:app/users/:user/data`

**Query Params**: 

**:app**: This is the APP ID, this is created when you create a new application.

**:user**: This time, you already have your userID.  

{{< notice tip >}}
Use the same userID as you use for your app or website.  This makes it easy to reference and retrieve data.   
{{</ notice >}}

**Required Headers**:

**x-rownd-app-key**: This is gathered directly from the App page. <a href="/getting-started/creating-an-app/">Learn how to create an App Key</a>


**x-rownd-app-secret**: You can only see the app key once prior to it disappearing.  If you lose the app secret, you must create a new App key/secret combo.  <a href="/getting-started/creating-an-app/">Learn how to see App Secret</a>

**Example cURL request**

```C#
curl --location --request GET 'https://api.rownd.io/applications/274488958387225088/users/newuser1235/data' \
--header 'x-rownd-app-secret: 6fe6c7c9956ee67c935e89d620b6c912aa12b2b17c1c6b97' \
--header 'x-rownd-app-key: 13b1f611-327f-478f-8c81-651d3dbae741'
```

**Example response**
Note the `RESTRICTED` field. 

```JSON
{
    "data": {
        "email": "testuser@test1.com",
        "first_name": "RESTRICTED",
        "last_name": "McAwesome",
        "cell_phone_number": "55555455555",
        "user_id": "newuser1235"
    }
}
```


#### Retrieve all data

This is very similar to the "single" data field.

`https://api.rownd.io/applications/:app/users/data`

**Query Params**: 

**:app**: This is the APP ID, this is created when you create an application.

**Required Headers**:

**x-rownd-app-key**: This is gathered directly from the App page. <a href="/getting-started/creating-an-app/">Learn how to create an App Key</a>


**x-rownd-app-secret**: You can only see the app key once prior to it disappearing.  If you lose the app secret, you must create a new App key/secret combo.  <a href="/getting-started/creating-an-app/">Learn how to see App Secret</a>

**Example cURL request**

```C#
curl --location --request GET 'https://api.rownd.io/applications/274488958387225088/users/data' \
--header 'x-rownd-app-secret: 6fe6c7c9956ee67c935e89d620b6c912aa12b2b17c1c6b97' \
--header 'x-rownd-app-key: 13b1f611-327f-478f-8c81-651d3dbae741'
```

**Example response**

This is an example response for 2 users. 

```JSON
{
    "total_results": 2,
    "results": [
        {
            "data": {
                "email": "testuser@test.com",
                "first_name": "RESTRICTED",
                "last_name": "Thelen",
                "cell_phone_number": "5555555555",
                "user_id": "274488958387225088-test-user"
            }
        },
        {
            "data": {
                "email": "testuser@test1.com",
                "first_name": "Robert",
                "last_name": "McAwesome",
                "cell_phone_number": "55555455555",
                "user_id": "newuser1235"
            }
        }
    ]
}
```

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/UNq2-fX0JNQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


