---
title: "Tutorial: How to use Rownd for forms"
date: 2020-06-03T16:37:34-04:00
weight: 11
draft: false
---

Prep, in this case, the form is all set up and the variables and buttons are referenced here in the `javascript`.  

Here we are going to gather first name, last name, and e-mail.  This required setting up a Rownd app, setting up data types, and getting a Rownd Personal Data Center button from `tools`. 

#### The setup and URL

{{< notice tip >}}
Rownd does not accept CORS, please ensure your API is being utilized on the backend as a best practice. 
{{</ notice >}}

The URL also includes 2 critical pieces of information, the `appID` and the `userID`.  You find the `appID` in the app section of the Rownd developer area. 

**Create a userID**

Here, we create it using the origin `waitlist` + `first_name` + `date`, etc, but you can use anything you want!  This is how you will ID the individual in the future - do not put PII into this name if possible.

**Pass form data to Rownd**
In this case, we are passing first_name, last_name, and e-mail.  

```javascript
          var data= {data:{
            first_name: $('#fname', $form).val(),
            last_name: $('#lname', $form).val(),
            email: $('#email', $form).val()
```
{{< notice tip >}}
The fields need to match what is in the App Personal Data Mapping fields.  
{{</ notice >}}

**What it looks like together**

```javascript
submitSuccess: function ($form, e) {
          e.preventDefault()
          var submitButton = $('#btn-submit-waitlist-form', $form)
          var userId="waitlist_"+$('#fname', $form).val()+ "_"+(+new Date).toString(36).slice(-5);
          var url = "https://api.rownd.io/applications/213131184382923998848/users/"+userId+"/data"
          var data= {data:{
            first_name: $('#fname', $form).val(),
            last_name: $('#lname', $form).val(),
            email: $('#email', $form).val()
```

#### Setting up the JS API Call 

`url` is from the variable above.

`data:` is created with the `var data` above. 

`headers:` are found in the Rownd Developer Platform.  
`
```javascript
$.ajax({
            type: 'PUT',
            // Set real action URL
            url: url,
            contentType: "application/json; charset=utf-8",
            data:JSON.stringify(data),
            headers: {
              'x-rownd-app-key': '516e4121-d5599-49d3-8020-a9e6268096654433',
              'x-rownd-app-secret': '42349e4add9b53fad359b57b7f6678793cad471245d72e46653'
            },
```

#### Put it all together...

Note, there is some button error handling at the end, not a requirement.  



```javascript
$('#waitlist-form').find('input').jqBootstrapValidation({
        preventSubmit: true,
        submitError: function ($form, event, errors) {
        },
        submitSuccess: function ($form, e) {
          e.preventDefault()
          var submitButton = $('#btn-submit-waitlist-form', $form)
          var userId="waitlist_"+$('#fname', $form).val()+ "_"+(+new Date).toString(36).slice(-5);
          var url = "https://api.rownd.io/applications/275991899286798848/users/"+userId+"/data"
          var data= {data:{
            first_name: $('#fname', $form).val(),
            last_name: $('#lname', $form).val(),
            email: $('#email', $form).val()
          }}
          $.ajax({
            type: 'PUT',
            // Set real action URL
            url: url,
            contentType: "application/json; charset=utf-8",
            data:JSON.stringify(data),
            headers: {
              'x-rownd-app-key': '516e4121-d554-49d3-8020-a9e626809665',
              'x-rownd-app-secret': '4234e9e4add9b53fad359b57b7f6678793cad471245d72e4'
            },
            beforeSend: function (xhr, opts) {
              submitButton.html('Please Wait...')
              submitButton.prop('disabled', 'disabled')
            }
          }).done(function (data) {
            submitButton.html( 'Submit')
            submitButton.prop('disabled', false)
            $form[0].reset();
            $('#success-text').removeClass('d-none');
            setTimeout(() => {
              $('#success-text').addClass('d-none');
            }, 10000);
            $('#waitlist-form').addClass('d-none');
            $('#thank-u-message').removeClass('d-none');
          })
        },
    ```



