---
title: "Airtable"
date: 2021-11-29T10:00:00+00:00
# weight: 1
icon: "ti-bolt" # themify icon pack : https://themify.me/themify-icons
description: "Rownd's Airtable Connector"
# type dont remove or customize
type : "docs"
# weight: "8"
---

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qomtRoT1nTA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
