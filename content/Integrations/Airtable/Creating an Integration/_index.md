---
title: "Creating an Integration"
date: 2021-11-29T10:00:00+00:00
# weight: 1
icon: "ti-bolt" # themify icon pack : https://themify.me/themify-icons
description: "Creating an Airtable Integration"
# type dont remove or customize
type : "docs"
weight: "1"
---

The Rownd Integration for Airtable lets you sync personal information between Rownd and a single Airtable Table. When configured, PII stored in the Table will be discoverable and manageable by the data owners.

---

### Creating a new Integration

1. From the **Integrations** tab in the sidebar, click on the **Add Integration** button. If this is your first time creating an integration, you'll already be in the Connector Catalog and don't need to click the button.
2. Choose the **Airtable** connector from the Connector Catalog
<img src="/images/airtable/connector-catalog.png" alt="Airtable in the Connector Catalog" class="rownd-image-shadow" />

3. Enter a name for your new Integration.
<img src="/images/airtable/overview.png" alt="Naming your Integration" class="rownd-image-shadow" />

After you've entered a descriptive name, click **Next**

4. Authenticate with Airtable

Enter an API key that Rownd can use for reading and writing to Airtable. You can create or retrieve your API key from the Airtable [account settings](https://airtable.com/account) under the **API** heading.

<img src="/images/airtable/authenticate.png" alt="authenticating with Airtable" class="rownd-image-shadow" />

Click the **Next** button after entering your API key.

5. Airtable Settings

Select the Airtable Base and Table

<img src="/images/airtable/settings.png" alt="Choosing the Airtable Settings" class="rownd-image-shadow" />

When done, click **Next**

6. Last Modified Time Field

In this step, Rownd will check for the existence of a <code>lastModifiedTime</code> Field within your chosen Table. This is a special field type that Airtable uses to record the last time an entry in the Table was updated. if necessary, please add this Field (with any name) to your Table and click the **Retry** button. Otherwise, click **Create** to finish.

<img src="/images/airtable/last-modified-field-failure.png" alt="Last Modified Time Field not found" class="rownd-image-shadow" />

<img src="/images/airtable/last-modified-field-success.png" alt="Last Modified Time Field found" class="rownd-image-shadow" />

For more inormation on adding a <code>lastModifiedTime</code> Field, please check out the [Airtable Documentation](https://support.airtable.com/hc/en-us/articles/360022745493-Last-modified-time-field)

Click the **Create** button to finish creating your Integration

##### Attach your Integration to a Rownd Application

After creating your Integration, you must attach it to an existing Rownd application.

1. From the Integrations table, click the overflow icon on your new Integration and select **Attach to application**
<img src="/images/rtdb-attach-overflow.png" alt="Attach your Integration to an application" class="rownd-image-shadow" />

2. Choose the application from the selector and click **Next**
<img src="/images/airtable/attach-choose-app.png" alt="Choose the application" class="rownd-image-shadow" />

3. Map data between the Rownd application and your new Integration

You will see a list of all fields that exist in your chosen application's schema. Choose the corresponding Field within your Airtable Table.

For instance, you could have a field in your Rownd application called `first_name` and a corresponding Field in your Table called `firstName`. Choose the `firstName` options in the dropdown next to `first_name`.

<img src="/images/airtable/attach-map-fields.png" alt="Map Rownd fields to your Intnegration schema" class="rownd-image-shadow" />

{{< notice info >}}
A mapping for the `email` field is required. Rownd uses this field to identify and search for users.
{{</ notice >}}

{{< notice tip >}}
Rownd can only manage fields for which you have provided a mapping. Therefore, define as many mappings as possible to get the most value from Rownd.
{{</ notice >}}

Once you complete the mapping, click **Save**.