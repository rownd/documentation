---
title: "User's Guide: Personal Data Center"
date: 2018-12-01T11:02:05+06:00
icon: "ti-home" # themify icon pack : https://themify.me/themify-icons
description: "Welcome to the future of data privacy and ownership.  The Personal Data Center."
# type dont remove or customize
type : "docs"
---

### Our mission
Our mission is simple, we believe EVERYONE deserves individual data ownership, privacy, and security.  

The first step to that vision is the Personal Data Center - a one stop shop for you to control your personal data.

Let's dig right in...

### A video walk through of the Rownd Personal Data Center ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qomtRoT1nTA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


