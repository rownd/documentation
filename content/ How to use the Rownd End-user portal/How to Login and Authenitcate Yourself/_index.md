---
title: "Claim and Manage your Data"
date: 2020-06-03T16:37:34-04:00
Weight: 12
draft: false
---

## How to claim your data
Rownd uses verifiable pieces of personal data to allow users to claim their data.  For most apps, this will be an e-mail address.  You have to use the same e-mail address you used to sign-up or in with the app to claim your data.  

### Verify E-mail
<img src="/images/verify-email.png" alt="Verify e-mail" style="width:800px">
Here, you simply enter the same e-mail address you used in the app or website for the data you want to claim.  

<img src="/images/enter-email.png" alt="Enter your e-mail" style="width:800px">
After you enter your mail, click on "Send verification e-mail".  Rownd will now send you an e-mail (may take a few seconds).  

{{< notice tip >}}
If you don't see the e-mail right away, check in your e-mail filters (promotions, social, or sometimes SPAM).  
{{</ notice >}}

After you get the e-mail, simply click on the verification link.

### See data the app is collecting

<img src="/images/data-overview.png" alt="List of all personal data the app has sent to Rownd for protection" style="width:800px">
You can now explore the different data types. The toggle can even turn off data from this overview.  To learn more about a particular piece of data, click on view.

### Data type in-depth
After you click view, you will be able to explore the data!

<img src="/images/data-type-deep.png" alt="First name deep dive" style="width:800px">

This is where the developer can show you what they are doing with the data and how it is being used.  

***Data Name*** This is the name of the data-type. Examples include email, First Name, Last Name, Address, etc

***Sharing*** Are you currently sharing the data?  You can revoke access to most data.  The app can put a minimum required retention period for data (up to a week), where the data cannot be revoked.  Data that is owned by the app (like App user name) cannot be revoked, but almost all PII can be.

***Download my Data*** You opportunity to download the data.  For most PII this is not significant, but, in the future, when shopping history, medical history, number of steps, etc is being managed on Rownd, this will be very valuable to easily move data.

***Edit*** Instead of logging back into an app to update a key piece of data, do it from here!  Select "edit" and you can change your name here. It also unlocks "Revoke after"

***Revoke After*** This is the "set and forget" part of data ownership we believe should be common place. The app owner/developer can set an automatic expiration for your data.  If that timeline does not suit your needs, then you can change it (ranges between a day and never).

***Will data be sold/shared to a 3rd party*** If the app owner selects "yes" here, they must explain where the data is going and why.

***Owned By*** Data owned by the end user (the user of the app, you), is always visible and editable.  If it is owned by the app, it cannot be edited and cannot be revoked.

***Justification for Collection*** The Data Processor (the app) must justify why they need to collect your data.  It is not merely enough to just want to, they have to sell it.  Not convinced?  Turn it off!

***Opt Out Consequences*** These explain what happens if you turn off this piece of data. Some may have little impact, some may cause the app/website to no longer work. An example of a breaking change would be removing your e-mail from an e-mail newsletter service...

### See all of your data
One of the coolest features of Rownd is the ability to manage all of your data from one place.  If an developer or company uses Rownd across multiple apps, you can see them all in one place.

<img src="/images/more-apps.png" alt="All of your apps in one place" style="width:800px">

### A video walk through of the Rownd Personal Data Center ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qomtRoT1nTA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





