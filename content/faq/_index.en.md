---
title: "Frequently Asked Questions"
description: "Questions that customers frequently ask."
draft: false
---

{{< faq "What is included in the free plan?" >}}
 The [starter plan](https://rownd.io/pricing) has all of the features needed to get started with Rownd, including a wide range of Personal data types to choose from, one application, one seat, and up to 50 users.  You can try it out and get savvy with how to use Rownd.  The cool news?  When you are ready to graduate to "teams," we make it easy and affordable!  
{{</ faq >}}

{{< faq "Discounts for students and non-profit organizations?" >}}
Yes! Contact us for more information.  
{{</ faq >}}

{{< faq "I am a US-based company in North Carolina. Do I have to worry about data privacy laws?" >}}  Yes! Every data privacy law is different.  CCPA follows Californians <a href="https://techmonitor.ai/policy/californias-new-privacy-laws-eu" target="_blank" rel="noopener noreferrer">across state lines.</a> European privacy laws focus on whether you provide goods or services to EU citizens (e.g., do you offer goods on your website that could ship to Europe?). Overall, simply being data privacy-centric and providing great privacy experiences regardless of location is a best practice! 
{{</ faq >}}

{{< faq "How do I get started with Rownd?" >}}
You can [create an account](https://app.rownd.io) and get started today. We'd love to talk to you further to discuss your interest in Rownd and specific use cases that we can help you solve. If you're open to a brief meeting, [schedule a consultation](https://calendly.com/rob-rownd/rownd-demo) today!
{{</ faq >}}
