---
title: "Data Ownership 101"
date: 2018-12-29T11:02:05+06:00
icon: "ti-blackboard" # themify icon pack : https://themify.me/themify-icons
description: "Learn more about why we started Rownd and why data ownership is a business necessity"
# type dont remove or customize
type : "docs"
# wieght: 2
---

### Data Privacy is the foundation of Trust on the internet

Rownd quickly enables you to allow your customers to manage their personal data. 99% of businesses should view personal and private data is a liability.  It is expensive to keep safe and even harder to manage with the new data privacy laws.  Why take on all of that responsibility if you are not going to profit from it?  Let Rownd do the heavy lifting! 

### About Rownd ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LqvJGPGUPvM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




