---
title: "What is Data Ownership"
date: 2020-05-03T16:37:34-04:00
draft: false
---

By embracing personal data ownership, businesses, organizations, and governments will find lower security risk, higher customer/user satisfaction, and, you will be able to ask your customers for MORE personal data, since they will trust you more.

At the end of the day, Personal Data Ownership is around TRUST.  Rownd helps you build trust quickly.  <a href="https://blog.rownd.io/your-customers-dont-trust-you-and-we-have-the-proof" target="_blank">Learn more</a> about how to build trust with data privacy. 


### Key Terms

<a name="end-user"></a> **End User**: An end-user is the person who is using an app or website.  This individual is also called the data-creator or the data-owner, depending on the data-privacy law.  The entire Rownd Platform revolves around this person.  

<a name="Personal Data Center"></a> **Personal Data Center**: This is where the end-user controls their data.  Also called the "Personal Data Center" 

<a name="Claim Personal Data"></a> **Claim Personal Data**: Rownd holds personal data and manages it on behalf of an end-user until they claim it.  Claiming personal data is a simple process of using a piece of verifiable data (right now, we use e-mail addresses).  As long as the e-mail address matches what the app and Rownd have and it is verified, the end-user gains access and control to their data.


