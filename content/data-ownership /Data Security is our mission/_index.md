---
title: "Security"
date: 2020-03-03T16:37:34-04:00
draft: false
---
### Data privacy and security go hand in hand

We truly believe data privacy and data security go hand in hand.  Because we built Rownd from the ground up with data privacy in mind (not data mining  or data exploitation), our entire platform  is stronger.

### Data is always encrypted (at rest and in transit)
Rownd utilizes the same encryption standards as banks and the US military to protect personal data (<a href="https://www.techopedia.com/definition/29703/256-bit-encryption#:~:text=256%2Dbit%20encryption%20is%20a,technologies%20including%20AES%20and%20SSL" target="_blank">256 bit</a>).  

The data is always encrypted at rest (in our database) and in transit.  

Rownd utilizes security best practices from end-to-end.  From how API Keys and secrets are generated and stored, to our security by design approach to our architecture.  Your data is safe.

### Fully insured against data breaches

In addition, Rownd is insured against data breach and cyber attack for $1M (Omissions and Errors) as a data processor and storer, a high bar, to prove our commitment to individual data privacy.  This is not just the standard "Data Breach" insurance around data breach responses, this is full liability insurance.  This insurance also forces us to have best practices around training, password management, access-control, and security. 