---
title: "Our Story"
date: 2020-06-03T16:37:34-04:00
draft: false
---

### Our Mission 
We believe everyone deserves individual data ownership, privacy, and security. 

### Our Vision 
Create a world where every person owns their personal data, everywhere.  Where they can manage who sees their data, can share it with anyone or any organization, and can revoke access just as easily.  A world where they can trade and sell their data and share in the profits of the knowledge economy.  A world of greater equality and greater mutual respect for our identities.  

### Our Why 
We believe in a future where everyone can truly own their own data. Where the person who creates data decides who or what may access that data. Ownership is the bedrock because until data is considered personal property it is impossible to have privacy and security. You could no more have personal physical privacy and security if you could never own your own belongings or room or home. The same principle applies to data.

Our mission is to accelerate the world toward true Data Ownership.

<br>

### How we plan on creating change

By making it easy for everyone!  The tools of the internet were created around data-mining and exploiting our data.  Only the largest corporations can afford to spend the time and money needed to be compliant with Data laws and move towards a data ownership model, but they have every incentive to defer investment and unleash lawyers instead of actual features. 

<br>

### Our product (our what)
Data Ownership and Privacy as a service.  We created Rownd to make it easy for any developer to add Data Privacy to a website or app quickly, while increasing security and increasing customer loyalty.  Check out the Rownd platform for yourself!

<br>

### Rob's story
Learn more about our founder's story here: <a href="https://blog.rownd.io/why-i-wanted-to-start-rownd/" target="_blank">Link</a>


**Every member of our team** has the same passion and a similar story - we all come from tech, we all have a passion for personal data protection, and we all have a personal reason why we are fighting for a new world.  Join us! 

