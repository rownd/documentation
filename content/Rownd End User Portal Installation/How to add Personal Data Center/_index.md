---
title: "How to add Personal Data Center"
date: 2020-06-03T16:37:34-04:00
weight: 12
draft: false
---

### Create a button or Link

<img src="/images/create-button.png" alt="Create a button or link" style="width:700px">

The Personal Data Center (PDC) is a key feature for Rownd and is included in all Rownd plans.  By showing that critical, personal, and private data is being protected and managed by a third party raises confidence and the ability to manage the data raises trust in the website or app.

We give you an easy to use Rownd button or you can simply use a link.  

#### Button 

Simply choose the size of the button, the app that you want linked to it, and then either click on the color of button you want or the `code` link and you will have a code snippit to add to your website.  The link will open a new tab/window when they click on it

#### Link

If you are using an app, simply use the link button to add data-ownership to any application.  The link will open Safari/Chrome (Apple/Android) and will start the data claiming process.


#### Under the hood

After clicking on the button, the end-user will have to claim their data (for first time Rownd users).  This involves validating a piece of data: e-mail or text (coming soon).   After validating the piece of data, they can then claim and manage their data. 

{{< notice tip >}}
 The logo, app name, and app description are all shown to the users at this point, so be sure to proof those areas in the applicaiton section prior to going live.    
{{</ notice >}}

You can also "try" the experience in the "Test" section below.


