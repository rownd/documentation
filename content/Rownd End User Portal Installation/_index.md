---
title: "Personal Data Center (PDC)"
date: 2018-12-03T11:02:05+06:00
icon: "ti-desktop" # themify icon pack : https://themify.me/themify-icons
description: "This is where the dream comes true.  The Personal Data Center allows end-users to control and manage their personal data"
# type dont remove or customize
type : "docs"
---

### The Rownd Personal Data Center creates trust in a world that needs it

This is where the dream comes true.  The Personal Data Center allows end-users to control and manage their personal data.

One simple button and apps become empowering, websites add trust.  The Manage my data button gives your users complete control over their PII. 

<img src="/images/manage-my-data-button.png" alt="Create instant trust" style="width:200px">
