---
title: "Creating an application"
date: 2020-06-04T16:37:34-04:00
draft: false
weight: 1
---
### Video tutorial ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RXfyZqAo4R0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Applications are a logical way to differentiate between users, apps, and websites.  An application can have up to 10 API keys.

Create a new app: Create a new application by clicking on "Add Application" and then you are must add an App title and description.  In addition, it is highly encouraged to add an app/website logo, this will add confidence to end-users as they use the Rownd Personal Data Center.  Please note that all three of these fields will be shown to end-users.  

{{< notice tip >}} The logo should be a square ratio to look the best for end-users {{</ notice >}}

<img src="/images/add-new-app.png" alt="New Application process" style="width:700px;justify:center">

API Keys: You can access, create, and delete API keys and secrets for each application from the Application page.  

{{< notice note >}} API Secret can only be seen once.  Please copy and safe-guard the secret. You can check the secret by clicking on the lock, but you cannot reset the secret.  If you lose the secret, you must create a new API key.{{</ notice >}}

<img src="/images/apikey-secret.png" alt="API Key Secret" style="width:700px;justify:center">

 {{< notice note >}} The number of apps is dependent upon the plan you are subscribed to.  {{</ notice >}}


