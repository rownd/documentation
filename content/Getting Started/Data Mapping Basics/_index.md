---
title: "Data mapping"
date: 2020-06-03T18:37:34-04:00
draft: false
weight: 5
---

### Walk Through ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/kiYrwLloOJw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Overview ###
Data mapping is a critical step to data ownership and privacy.  The end-user (the user of the app or websites) needs to understand what data is being held on them and why.  Rownd makes this easy with Data Mapping.  

Navigate to Data Mapping on the left side.  The Data types will be orgaized by apps.  The more apps you have the more data cards you will have.  

An important aspect of data ownership is how end-users gain access to their personal data.  Rownd uses a piece of shared data to do this, in this case, e-mail address.  For an end-user to claim and manage their data, they must first validate their e-mail.  

{{< notice note >}}
 At this time, every app must include an e-mail data-type, this is the only mechanism for user-validation.  In the future, Rownd will also support mobile phone number and allow apps/websites to pass through a user-ID.  
{{</ notice >}}

### Add Data Type ###

<img src="/images/data-mapping-overview.png" alt="Data Mapping Top Level" style="width:800px">

To add a data type, click on 'Add Data'.

### Data Mapping walk-through ###

<img src="/images/data-mapping-indepth.png" alt="Data Mapping drill down" style="width:800px">

**Field name**: This is the machine readable (used in APIs, etc) name of the data type.  
{{< notice note >}}
  The number of data types and the out of the box data types are determined by the Rownd plan you are on.  
{{</ notice >}}
<br>

**Display Name**: What the end-user sees for each data type. This field is editable

**Type**: Preset - for your information only.  In the enterprise plan, you can create custom data types and edit this field.  

**Required Retention**: This is how long a piece of data "locked" in Rownd.  The data can be edited via API/SDK, but an end-user cannot edit or turn off a piece of data until this retention period has passed.  This is to ensure the data is available for the primary use.  

{{< notice tip >}}
This required retention period was added to reduce the need to backup or store PII in a second database.  The data is guaranteed to be available for at least an hour and up to a week for shipping, delivery, or other use-cases.  
{{</ notice >}}

**Revoke By**: How long a piece of data is accessible to an application or website.  The value can be edited.    This is a critical part of GDPR and other data privacy laws. 

**Owned by:**: `Owned by app` cannot be turned off or deleted. This is data that is created BY the app or website and is owned by the organization. Some examples are userID, number of visits, and shopping lists, etc.  Although this data is technically owned by the organization, allowing the end-user to see the data that is being collected on them gives them more transparency and builds more trust.   `Owned by user` is personal data that is owned by the end user.  Examples are name, home address, demographic information, etc.  This also includes any data that is introspected based on big data insights.  Data owned by the end user can be edited, revoked, and deleted.  

{{< notice tip >}}
  Transparency is key.  Your users will feel more trust and goodwill towards you app if you show them what you are collecting on them.  Most will actually feel better about the experience.  In addition, you can actually ask for more relevant data at this point (if it gives them a better experience - never ask for data just to ask for it).
{{</ notice >}}

**Visible to user**: Visibility to set by default on most PII. Data "Owned by" the end-user is locked into visible to end-user.  Some examples of data that may not be visible is app owned data like UUID, App ID, etc.  

**Required**: When required is checked, any API call that is adding data must include those fields the first time.  This is useful to ensure a form is sending required data.  We will validate and ensure data is added for those fields.  

**Justification**: Why are you collecting this piece of data.  If you do not have a business or organizational need for the data, don't collect it!  Most Data Privacy laws are very blunt about this point: You can only collect data you need.  This justification ensures internally you have a reason why you are requesting a piece of data and what it will be used for and this field will be shared with the end-user in the Personal Data Center.

{{< notice tip >}}
  Be short and to the point.  If it takes 2 paragraphs to explain why you need a piece of data, you have to revisit your data-ownership strategy.  
{{</ notice >}}


**Will Data be sold/shared**: You also need to be transparent about what else you are going to do with the data.  Are you turning over this data to a marketing firm?  Will it be sold or traded?  You have every right to do just that, but you must be transparent.  If you have no plans to sell the data, just put `no`.

**Custom Opt Out Warning**: This is displayed to a customer prior to revoking access to a piece of personal data.  For example, if you use a user's first name to greet them in your monthly e-mail and they are about to turn-it-off, let them know that future e-mails will not be personalized.  On the more severe side, if they turn off a piece of critical data, like an e-mail address or an address for deliveries, let them know that your organization will no longer be able to conduct business as usual until that data is re-entered or turned back on.  

{{< notice tip >}}
  It is okay that a person turns off a piece of critical data, think of this as a HUGE time savings.  They would have requested you do the same thing, but in this case, they did it for you. 
{{</ notice >}}


### Video tutorial ###

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/e0bxf-HKqto" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>