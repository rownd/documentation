---
title: "Creating a form"
date: 2021-07-12T12:35:05-04:00
description: "Rownd Forms is a low-code/no-code way of securely collecting and storing data from users"
weight: 10
---

Rownd Forms offers a simple way to collect and store data from users, and it's as flexible as you need it to be! Each form is connected to a Rownd Application, so [create an app](/getting-started/creating-an-application) if you haven't already.

### Build your first form
Once you've created an application, it's quick and easy to start building your first form.

1. Open the Rownd application to which the form should be connected, then select the **Forms** tab.
2. Click **Create new form** to open the form creation panel.
3. Enter a title for the form, customize the submit button text if desired, then select the data types that should appear on the form. As you build the form, a live preview will be displayed.
    {{< notice note >}}You can click and drag the fields in the form preview to customize the order in which they appear.{{</ notice >}}
    <img src="/images/form-creation.png" alt="Creating a new form" class="rownd-image-shadow" />
4. When satisfied with the form, click **Save form** to create the form. You can always come back later to further customize it.

### Inserting / embedding a form into your website
Once a form has been created, you can embed the form on your website. The form will include certain styles dictated by Rownd, while also attempting to match your website's design so that it feels like a natural part of the site.

{{< notice tip >}}If you want to maintain more control over your form's layout, look, and feel, see the topic on [connecting to an existing form](#connect-to-an-existing-form).{{</ notice >}}

1. If you're not already there, navigate to the **Forms** tab of your Rownd application.
2. From the list of forms, locate the form you want to embed and press] **Show form code**. A new dialog will appear.
3. Within the "Install form code" dialog, you'll see two code blocks.
   <img src="/images/form-code.png" alt="Installing form code" class="rownd-image-shadow" />
4. Copy the first block and paste it into your website _where you want the form to appear_.
5. Copy the second block and paste it into your website just before the closing `</body>` tag.
   {{< notice info >}}If you're using a visual website builder, content management system, or similar website authoring tool (e.g. Wordpress, Netlify, Webflow, Wix, etc.), there may be a recommended field where you can paste this type of code. For more information, search your provider's documentation for `body tag` or a similar term.{{</ notice >}}
   {{< notice warning >}}Only insert the closing body tag code **once** in a given page, regardless of how many forms you embed. Inserting this code multiple times will cause unexpected behavior.{{</ notice >}}
6. Once the code has been inserted, publish your site to view the live form result.

{{< notice info >}}
The form code is dynamic, so if you change the form within Rownd, those changes will immediately be reflected on your website. 
{{</ notice >}}

<a name="connect-to-an-existing-form"></a>
### Connecting to an existing form
When you have an existing form that you want to manage with Rownd, or if you need a highly customized form that tightly integrates into your website's design, you can connect your own form code to Rownd for data storage and management.

Follow the process outlined above in "Inserting / embedding a form into your website" to set up the connection to Rownd. Since you may have an existing `<form>` tag, you can simply add the `data-rownd-*` attributes to it.

Next, add the attribute `data-rownd-form-custom` to the form tag. This will tell Rownd that the form is a custom form and not one that was created by Rownd.

The final form tag will look something like this:
```html

<form data-rownd-form="303930499900899908" data-rownd-form-key="7ba7b419-437b-488b-80e0-7e22598b1b1a" data-rownd-form-custom>
```

At this point, the form may work as-is; however, you'll want to check two things:
1. Any data that should be saved within Rownd must match the data field names that Rownd expects. Review your Rownd application's schema of data types within the Rownd dashboard to verify each field's name (e.g. `first_name`, `last_name`, `email`, etc.).
2. Rownd will collect the data by listening to the form's `submit` event. Ensure that you are calling `form.submit()` via a submit button or programmatically, or the data collection will not occur.

### Help and feedback
If you're having any trouble with Forms, or if you have feedback you'd like to share, please [reach out to us](mailto:support@rownd.io). We'd love to hear from you! 

